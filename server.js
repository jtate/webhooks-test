var bodyParser = require('body-parser');
var express = require('express');
var request = require('request');

var url ='http://requestb.in/19luynd1';

var app = express();

// create application/json parser
var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({
  extended: false
});

app.set('port', (process.env.PORT || 5000));

app.get('/get', function(req, res) {
  request.post(url, {form:
    {message: 'oh hi! provided by heroku.'}
  });
  res.send('hi again!');
});

app.post('/post', jsonParser, function (req, res) {
  if (!req.body) {
    return res.sendStatus(400);
  }
  res.send(req.body);
});

app.get('/splunk', function(req, res) {
  request.post(url, {form:
    {event: 'testing the splunks'}
  });
  res.json('testing the splunk');
});


app.listen(app.get('port'));

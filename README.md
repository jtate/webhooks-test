# What?

For testing a get that makes a post! And other things.

## Running It

You can probably run this anywhere. This example, however, has only been tested with [Heroku](heroku.com/home).

## Helpers

RequestBin can help with Webhooks: http://requestb.in/.

Hurl.it can help with making HTTP requests: https://www.hurl.it/.
